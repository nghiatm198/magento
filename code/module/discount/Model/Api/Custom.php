<?php
 
namespace module\discount\Model\Api;
 
use Psr\Log\LoggerInterface;
 
class Custom 
{
    protected $logger;
 
    public function __construct(
        LoggerInterface $logger
    )
    {
 
        $this->logger = $logger;
    }
 
    /**
     * @inheritdoc
     */
 
    public function getPost($value)
    {
        $response = ['success' => false];
 
        try {
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
	    $resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
	    $connection = $resource->getConnection();
	    $tableName = $resource->getTableName('custom_discount'); 

		$sql = "UPDATE " . $tableName ." SET value_discount = ". $value ." WHERE key_discount = 'discount'";
		$connection->query($sql); 
		$result = "update discount value success";


            $response = ['success' => true, 'message' => $result];
        } catch (\Exception $e) {
            $response = ['success' => false, 'message' => $e->getMessage()];
            $this->logger->info($e->getMessage());
        }
        $returnArray = json_encode($response);
        return $returnArray; 
    }
}