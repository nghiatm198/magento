/*
 Navicat Premium Data Transfer

 Source Server         : magento
 Source Server Type    : MySQL
 Source Server Version : 100325
 Source Host           : localhost:3306
 Source Schema         : magento2

 Target Server Type    : MySQL
 Target Server Version : 100325
 File Encoding         : 65001

 Date: 06/11/2020 02:09:14
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for custom_discount
-- ----------------------------
DROP TABLE IF EXISTS `custom_discount`;
CREATE TABLE `custom_discount`  (
  `value_discount` int(255) NULL DEFAULT NULL,
  `key_discount` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of custom_discount
-- ----------------------------
INSERT INTO `custom_discount` VALUES (15, 'discount');

SET FOREIGN_KEY_CHECKS = 1;
